import React from 'react';
import circleErrorIcon from './circle-error.svg';
import searchIcon from './search-icon.svg';
import nothingFoundIcon from './nothing-found.png';
import './BotList.scss';
import axios from 'axios';
import moment from 'moment';
import classNames from 'classnames';
import botJson from './load_bots.json';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

class BotList extends React.Component {
  state = {
    botList: botJson, 
    botListToDisplay: botJson, 
    loading: true, 
    error: null,
    filterString: ''
  };

  componentDidMount() {
    this.reload();
  }

  reload = async e => {
    e && e.preventDefault();

    try {
      this.setState({ loading: true, error: null });

      // let botJson = await axios.get('https://new.cs.money/2.0/load_bots');
      await sleep(2000);

      if (Math.floor(Math.random() * 2)) {
        throw new Error('Some error');
      }

      let botList = botJson.map(bot => ({...bot, reg: moment(bot.reg).format('DD.MM.YYYY')}));

      this.setState({ 
        loading: false, 
        botList,
      }, this.filterResults);
    } catch (err) {
      console.log('error', err);
      this.setState({ error: err.message, loading: false });
    }
  }

  showAll = e => {
    e && e.preventDefault();

    this.setState({
      filterString: '',
    }, this.filterResults);
  }

  inputChange = e => {
    const filterString = e.target.value.toLowerCase();

    this.setState({ 
      filterString, 
    }, this.filterResults);
  };
  
  filterResults = () => {
    const filterString = this.state.filterString.toLowerCase();

    this.setState({ 
      botListToDisplay: this.state.botList.filter(bot => bot.name.toLowerCase().includes(filterString)) 
    });
  };

  clearInput = e => {
    e && e.preventDefault();

    this.setState({ 
      filterString: '', 
    }, this.filterResults);
  }

  render() {
    let { botList, loading, error, botListToDisplay, filterString } = this.state;

    return (
      <div className='bots-list'>
        <div className='bots-list__title'>List of bots</div>
        <div className='bots-list__search'>
          <div><img src={searchIcon} alt="search" /></div>
          <div className='bots-list__search-input'><input value={filterString} onChange={this.inputChange} type='text' placeholder='Searching for a bot…' /></div>
          <div><a onClick={this.clearInput}>X</a></div>
        </div>
        <div className={classNames('bots-list__table', { loading })}>
          <table>
            <thead>
              <tr><th>ID</th><th>Nickname</th><th>Registration</th><th>Level</th></tr>
            </thead>
            {!error && (botListToDisplay.length || '') && (<tbody>
              {botListToDisplay.map(bot => (
                <tr key={bot.id}>
                  <td><span>{bot.id}</span></td>
                  <td><a href={bot.profileLinkId}>{bot.name}</a></td>
                  <td><span>{bot.reg}</span></td>
                  <td><span>{bot.level}</span></td>
                </tr>
              ))}
            </tbody>)}
          </table>
        </div>
        {error && <div className='bots-list__error'>
          <div className='bots-list__error-image'><img src={circleErrorIcon} alt="search" /></div>
          <div className='bots-list__error-title'>Loading error</div>
          <div className='bots-list__error-message'>Something went wrong, try again later</div>
          <div className='bots-list__error-reload'><a href='' onClick={this.reload}>RELOAD</a></div>
        </div>}
        {!botListToDisplay.length && <div className='bots-list__empty'>
          <div className='bots-list__empty-image'><img src={nothingFoundIcon} alt="Nothing found" /></div>
          <div className='bots-list__empty-title'>Nothing found</div>
              <div className='bots-list__empty-message'>Currently available {botList.length} bots</div>
          <div className='bots-list__empty-reload'><a href='' onClick={this.showAll}>SHOW ALL</a></div>
        </div>}
      </div>
    );
  }
}

export default BotList;
